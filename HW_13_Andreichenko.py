# 1. Подключитесь к API НБУ ( документация тут https://bank.gov.ua/ua/open-data/api-dev ),
# получите текущий курс валют и запишите его в TXT-файл в таком формате:
#  "[дата создания запроса]"
# 1. [название ввалюты 1] to UAH: [значение курса к валюте 1]
# 2. [название ввалюты 2] to UAH: [значение курса к валюте 2]
# 3. [название ввалюты 3] to UAH: [значение курса к валюте 3]
# ...
# n. [название ввалюты n] to UAH: [значение курса к валюте n]
#

# 2. Пользователь вводит название валюты и дату, программа возвращает пользователю курс гривны к этой валюте за
# указаную дату используя API НБУ. Формат ввода пользователем данных - на ваше усмотрение. Реализовать с помощью ООП!

import re
import requests
from datetime import date

today = date.today()
today_str = today.strftime("%d.%m.%Y")


def find_currency(filepath, file_text, user_date, user_currency):
    if user_date in file_text:
        with open(filepath, "r") as inp:
            lines = inp.readlines()
            for item in iter(lines):
                if user_currency in item:
                    return item
            return "No desired currency"
    else:
        return "No desired date."


class ToFile:

    def __init__(self, filepath):
        if isinstance(filepath, str):
            self._filepath = filepath
        else:
            print("Type mismatch error occurred", TypeError)
            raise TypeError

    def write_to_file(self, currencies_dict):
        str_dict = str(currencies_dict)
        with open(self._filepath, "w") as file:
            file.writelines(today_str + "\n\r")
            i = 1
            for key, value in currencies_dict.items():
                file.writelines(f"{i}: " + str(key) + " to UAH " + str(value) + "\n\r")
                i += 1
        print("\nWritten successfully!")

    def read_from_file(self):
        with open(self._filepath, "r") as file:
            file_text = file.read()
        return file_text


class TryConnection:

    def __init__(self, url):
        if isinstance(url, str):
            self._url = url
        else:
            print("Type mismatch error occurred", TypeError)
            raise TypeError

    def try_connect(self):
        try:
            res = requests.get(self._url)
        except:
            print("Exception occurred")
        else:
            print("Content-Type:", res.headers["Content-Type"])
            print("Status-code:", res.status_code)
            if 300 > res.status_code >= 200 and res.headers["Content-Type"] == "application/json; charset=utf-8":
                try:
                    json_content = res.json()
                except:
                    print("Json exception")
                else:
                    print("Json_content:", json_content)

                    currencies_dict = {}

                    for dict_item in json_content:
                        currencies_dict[dict_item["txt"]] = dict_item["rate"]

                    print("Currencies content = ", currencies_dict)
                    return currencies_dict
            else:
                print("Status code error OR content-type error...")


def main():
    nbu_currency = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
    filepath = "nbu_currencies.txt"

    connect = TryConnection(nbu_currency)
    tofile = ToFile(filepath)

    currencies_dict = connect.try_connect()
    tofile.write_to_file(currencies_dict)
    file_text = tofile.read_from_file()

    print("Input date (format: dd.mm.yyyy) and currency (use ukrainian) to check hryvnia exchange rate:")
    print("Date is:", end=" ")
    user_date = input()
    print("\nCurrency is:", end=" ")
    user_currency = input()
    print("\n")

    date_format = re.compile('\d{1,2}\.\d{1,2}\.\d{2,4}$')
    if date_format.match(user_date):
        print("Info: \n", find_currency(filepath, file_text, user_date, user_currency))
    else:
        print("Invalid date format input.")


main()
